# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# coding: utf-8

Admin.create(email: "m.b.tell.me@gmail.com", password: "yuya0101")

20.times do |no|
  Post.create(title:"seed#{no}", content:"初期投入データ#{no}", 
              thumb: "/uploads/image/image/1/logo.jpg", 
              description:"初期投入description#{no}", keywords:"keywords#{no}")
  Category.create(name: "カテゴリー#{no}", thumb: "/uploads/image/image/1/logo.jpg")
end

CategoryPost.create(post_id: 1, category_id: 1)
CategoryPost.create(post_id: 1, category_id: 2)
CategoryPost.create(post_id: 3, category_id: 3)
CategoryPost.create(post_id: 4, category_id: 3)
CategoryPost.create(post_id: 5, category_id: 3)

CategoryPost.create(post_id: 6, category_id: 6)
CategoryPost.create(post_id: 7, category_id: 7)
CategoryPost.create(post_id: 8, category_id: 8)
CategoryPost.create(post_id: 9, category_id: 9)
CategoryPost.create(post_id: 10, category_id: 10)
CategoryPost.create(post_id: 11, category_id: 11)
CategoryPost.create(post_id: 12, category_id: 12)
CategoryPost.create(post_id: 13, category_id: 13)
CategoryPost.create(post_id: 14, category_id: 14)
CategoryPost.create(post_id: 15, category_id: 15)
CategoryPost.create(post_id: 16, category_id: 16)
CategoryPost.create(post_id: 17, category_id: 17)
CategoryPost.create(post_id: 18, category_id: 18)
CategoryPost.create(post_id: 19, category_id: 19)
CategoryPost.create(post_id: 20, category_id: 20)





