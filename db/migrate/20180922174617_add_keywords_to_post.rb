class AddKeywordsToPost < ActiveRecord::Migration[5.1]
  def change
    add_column :posts, :keywords, :text
  end
end
