class AddThumbToPost < ActiveRecord::Migration[5.1]
  def change
    add_column :posts, :thumb, :text
  end
end
