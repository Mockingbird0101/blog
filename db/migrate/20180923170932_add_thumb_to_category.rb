class AddThumbToCategory < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :thumb, :text
  end
end
