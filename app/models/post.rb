class Post < ApplicationRecord
  has_many :comments, dependent: :destroy
  has_many :category_posts, dependent: :destroy
  has_many :categories, through: :category_posts
  validates :title, :content, :description, :keywords, presence: true
  validates :description, length: { maximum: 255 }
end
