module ApplicationHelper
  
  # meta_tagに関するヘルパー
  def default_meta_tags
    {
      site: "Green-Onyx",
      title: "",
      reverse: true,
      charset: "utf-8",
      description: "web制作に関してほとんど知識が無かったrails初心者が自身のアウトプットの為に作ったブログです。",
      keywords: "Green-Onyx,green-onyx,birdman,プログラミング",
      canonical: request.original_url,
      separator: '|',
      icon: [
        { href: image_url("favicon.ico") },
        { href: image_url("icon.jpg"), rel: "apple-touch-icon", sizes: '180x180', type: "image/jpg" },
      ],
      og: {
        site_name: "Green-Onyx",
        title: "Green-Onyx",
        description: "web制作に関してほとんど知識が無かったrails初心者が自身のアウトプットの為に作ったブログです。",
        type: 'blog',
        url: request.original_url,
        image: image_url("ogp.png"),
        locale: "ja_JP",
      },
      twitter: {
        card: "summary",
        site: "@Birdman_yk_",
        creator: "@Birdman_yk_",
        domain: "8green-onyx8.com",
        title: "Green-Onyx",
        description: "web制作に関してほとんど知識が無かったrails初心者が自身のアウトプットの為に作ったブログです。",
      }
    }
  end
  
  # 年月日時分のみ表示するヘルパー
  def simple_time(time)
    time.strftime("%Y-%m-%d %H:%M ")
  end
end
