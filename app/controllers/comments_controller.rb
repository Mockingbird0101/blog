class CommentsController < ApplicationController

  def create
    @post = Post.find(params[:post_id])
    @categories = @post.categories.order('created_at DESC')
    @comment = @post.comments.build(comment_params)
    if @comment.save && admin_signed_in?
      redirect_to post_path(@comment.post_id)
    elsif admin_signed_in?
      @comments = @post.comments.order('created_at DESC')
      render 'posts/show' 
    elsif @comment.save
      redirect_to post_path(@comment.post_id)
    else
      @comments = @post.comments.order('created_at DESC')
      render 'posts/show'
    end
  end
  
  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    @post = Post.find(params[:post_id])
    redirect_to post_path(@post)
  end
  
  
  private
    def comment_params
      params.require(:comment).permit(:post_id, :name, :content )
    end
end
