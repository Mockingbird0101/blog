class PostsController < ApplicationController
  before_action :authenticate_admin!, only: [:new, :create, :edit, :update, :destroy]
  
  def index
    @post = Post.page(params[:page]).per(10).order('created_at DESC')
  end

  def show
    @post = Post.find(params[:id])
    @comment = Comment.new
    @comments = @post.comments.order('created_at DESC')
    @categories = @post.categories.order('created_at DESC')
    @url = request.url
  end

  def new
    @post = Post.new
    @post.category_posts.build
  end
  
  def create
    @post = Post.create(post_params)
    if @post.save then
      redirect_to posts_path
    else
      render 'new'
    end
  end
  
  def edit
    @post = Post.find(params[:id])
  end
  
  def update
    @post = Post.find(params[:id])
    if @post.update(post_params) then
      redirect_to post_path
    else
      render 'edit'
    end
  end
  
  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    redirect_to posts_path
  end
  
  def after_sign_in_path_for(resource)
      posts_path
  end
  
  
  private
  def post_params
    params.require(:post).permit(:title, :content, :thumb, :description, :keywords, { :category_ids=> [] })
  end
end
