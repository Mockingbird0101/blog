# conohaのサーバーのIP、ログインするユーザー名、サーバーの役割
# xxxの部分はサーバーのIPアドレス
# 10022はポートを変更している場合。通常は22
server '118.27.15.116', user: 'admin', roles: %w{app db web}, port: 50022 

#デプロイするサーバーにsshログインする鍵の情報。
set :ssh_options, keys: '~/.ssh/id_rsa'