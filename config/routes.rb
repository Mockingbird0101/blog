Rails.application.routes.draw do

  devise_for :admins
  resources :posts do
    resources :comments
  end
  resources :categories
  resources :images
  root 'posts#index'

end
